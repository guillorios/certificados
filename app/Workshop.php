<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Workshop extends Model
{

    public function certificates(){
        return $this->hasOne('App\Certificate');
    }
}
