<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        setlocale(LC_TIME, config('app.locale'));
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
	    Schema::defaultStringLength(191);

        Carbon::setLocale(config('app.locale'));
        Carbon::setUTF8(true);

        //setlocale(LC_TIME, config('app.locale'));
        setlocale(LC_TIME, 'es_ES.utf8');

        DB::statement("SET lc_time_names = 'es_ES'");

    }
}
