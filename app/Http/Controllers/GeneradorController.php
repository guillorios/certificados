<?php

namespace App\Http\Controllers;

use App\Topic;
use Carbon\Carbon;
use App\Certificate;
use Illuminate\Http\Request;
use setasign\Fpdi\Fpdi;

use RealRashid\SweetAlert\Facades\Alert;

class GeneradorController extends Controller
{
    //

	public function imprimir(){
  		$today = Carbon::now()->format('d/m/Y');
  		$pdf = \PDF::loadView('ejemplo', compact('today'));
  		return $pdf->download('ejemplo.pdf');
    }


    public function index(Request $request){

        $certificates = Certificate::Search($request->identified, $request->topic_id)->get();

        if ($certificates->count()>0){

            $certificate = $certificates->first();
            $pdf = \PDF::loadView('certificado', compact('certificate'));

            if ($certificate->topic->orientation == 'VERTICAL'){
                return $pdf->setPaper('lether', 'portrait')
                        ->download($certificate->topic->name . '_' . $certificate->identified . '.pdf');
            }else{
                return $pdf->setPaper('lether', 'landscape')
                        ->download($certificate->topic->name . '_' . $certificate->identified . '.pdf');
            }

        }
        else{

            $topics = Topic::all();

            alert('Exito', 'Permisos asignados', 'success');
            /* Alert::warning('Alerta', 'El Documento no se encuentra registrado, por favor comunicarse con el administrador.'); */
            alert()->error('Error','El Documento no se encuentra registrado, por favor comunicarse con el administrador.');
            return view('welcome', [
                'topics' => $topics,
            ]);

        }



    }



}
