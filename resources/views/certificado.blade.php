<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Documento</title>
        <style>

        body{
            background-image: url({{ Voyager::image($certificate->topic->image)}});
            background-position: center;
            background-repeat: no-repeat;
            background-size: 100%;
            width: 100%;
            height: 100%;
            padding: 0px;
            margin: 0px;
        }
        html {
	        margin: 0pt 0pt;
        }
        .contenido{
            font-size: 20px;
        }
        #primero{
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
            font-size: 28px;
            font-weight:bold;
            padding-top: {{ $certificate->topic->name_padding_top }}px;
        }
        #segundo{
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
            padding-top: {{ $certificate->topic->identified_padding_top }}px;
        }
        #tercero{
            font-family: Arial, Helvetica, sans-serif;
            font-weight:bold;
            text-align: center;
            padding-top: {{ $certificate->topic->subtitle_padding_top }}px;
        }
        #cuarto{
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
            padding-top: {{ $certificate->topic->period_padding_top }}px;
            line-height:3px;
        }
        #quinto{
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
            line-height:3px;
        }


    </style>
    </head>
    <body>
        <div class="contenido">
            <p id="primero">{{ $certificate->name }} </p>
            <p id="segundo">Con {{ $certificate->identified_id }} No. {{ $certificate->identified }} </p>
            @if ($certificate->topic->subtitle == 1)
                <p id="tercero">{{ $certificate->workshop->name }} </p>
            @endif
            @if ($certificate->topic->period == 1)
                <p id="cuarto">Que tuvo lugar en {{ $certificate->city->name }} </p>
                <p id="quinto">del {{ $certificate->work_ini->formatLocalized('%d de %B') }} al {{ $certificate->work_fin->formatLocalized('%d de %B de %Y') }},</p>
                <p id="quinto">con una intensidad de 20 horas.</p>
            @endif
        </div>
    </body>
</html>


