<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class City extends Model
{

    public function certificates(){
        return $this->hasOne('App\Certificate');
    }
}
