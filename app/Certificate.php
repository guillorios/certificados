<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    //

    protected $dates = ['work_ini', 'work_fin'];

    public function scopeSearch($query, $identified, $topic){

        return $query
                ->where('identified', '=', $identified)
                ->where('topic_id', '=', $topic);

    }

    public function topic(){
        return $this->belongsTo('App\Topic');
    }

    public function workshop(){
        return $this->belongsTo('App\Workshop');
    }

    public function city(){
        return $this->belongsTo('App\City');
    }

}
